#define _GNU_SOURCE 1

#include<stdlib.h>
#include<stdio.h>
#include <string.h>
#include <unistd.h>
#include<sys/mman.h>


const unsigned long long PAGE_ALIGNMENT_MASK = ~0xFFF;

void test_malloc_and_mremap();
void test_mmap_and_mremap();

//
// http://stackoverflow.com/questions/2679065/can-a-memory-page-be-moved-by-modifying-the-page-table
//
// ATTENTION: mremap does not work with malloced data.
//
int main(int argc, char* argv[])
{
   test_mmap_and_mremap();
   printf("Done\n");
   return 0;
}

void test_malloc_and_mremap()
{
   int PAGE_SIZE = getpagesize();
   int allocNumBytes = PAGE_SIZE * 3;
   char* oldM = malloc(allocNumBytes);
   char* newM = malloc(allocNumBytes);

   void* oldMPageAddress = (void*)((unsigned long long) oldM & PAGE_ALIGNMENT_MASK);
   void* newMPageAddress = (void*)((unsigned long long) newM & PAGE_ALIGNMENT_MASK);
   void* mremapResult = NULL;

   printf("ATTENTION: mremap does not work with malloced data.\n");
   printf("Test 1\n");

   printf("Before alignment\n");
   printf("oldM            = %p\n", oldM);
   printf("oldPageAdddress = %p\n", oldMPageAddress);
   printf("newM            = %p\n", newM);
   printf("newPageAddress  = %p\n", newMPageAddress);

   //
   // It is possible that the malloc returns non-aligned memory
   // In this case we align it
   //
   if (oldM != oldMPageAddress)
   {
      oldMPageAddress += PAGE_SIZE;
      oldM = oldMPageAddress;
   }
   if (newM != newMPageAddress)
   {
      newMPageAddress += PAGE_SIZE;
      newM = newMPageAddress;
   }

   printf("After alignment\n");

   memset(oldM, 65, PAGE_SIZE);
   memset(newM, 66, PAGE_SIZE);

   printf("*oldM           = %c\n", oldM[0]);
   printf("oldM            = %p\n", oldM);
   printf("oldPageAdddress = %p\n", oldMPageAddress);
   printf("*newM           = %c\n", newM[0]);
   printf("newM            = %p\n", newM);
   printf("newPageAddress  = %p\n", newMPageAddress);

   mremapResult = mremap(oldMPageAddress, PAGE_SIZE, PAGE_SIZE, MREMAP_FIXED | MREMAP_MAYMOVE, newMPageAddress);

   printf("After mremap\n");
   printf("mremapResult    = %p\n", mremapResult);

   printf("*oldM           = %c\n", oldM[0]);
   printf("oldM            = %p\n", oldM);
   printf("oldPageAdddress = %p\n", oldMPageAddress);
   printf("*newM           = %c\n", newM[0]);
   printf("newM            = %p\n", newM);
   printf("newPageAddress  = %p\n", newMPageAddress);

   printf("Done: Test 1\n");
}

void test_mmap_and_mremap()
{
   int PAGE_SIZE = getpagesize();
   char* m = NULL;
   char* mA = NULL, mB = NULL, mC = NULL;
   void* temp;

   printf("Test 2\n");
   printf("page size = %d\n", PAGE_SIZE);

   m = (char*)mmap(0, PAGE_SIZE*3, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
   temp = m+PAGE_SIZE*2;

   memset(m, 65, PAGE_SIZE);
   memset(m+PAGE_SIZE, 66, PAGE_SIZE);

   printf("before data %c %c\n", m[0], m[PAGE_SIZE]);
   printf("before m %p\n", m);

   mremap(m + PAGE_SIZE, PAGE_SIZE, PAGE_SIZE, MREMAP_FIXED | MREMAP_MAYMOVE, temp);
   mremap(m, PAGE_SIZE, PAGE_SIZE, MREMAP_FIXED | MREMAP_MAYMOVE, m+PAGE_SIZE);
   mremap(temp, PAGE_SIZE, PAGE_SIZE, MREMAP_FIXED | MREMAP_MAYMOVE, m);

   printf("after  data %c %c\n", m[0], m[PAGE_SIZE]);
   printf("after  m %p\n", m);
   printf("Done: Test 2\n");
}

